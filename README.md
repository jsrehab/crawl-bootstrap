# Bootstrap crawl
This repository contains the crawl script that uses
`js-ui-framework-detection-webext` (found in this repository group) to collect
[Bootstrap](https://getbootstrap.com/) usage data.

## License
Licensed under the MIT license (see LICENSE.txt)

Copyright (c) 2022 Inria
