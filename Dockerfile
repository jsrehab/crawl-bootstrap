# docker build --build-arg crawltar=crawl_1614848398.tar.gz -t "crawl:crawl" .

FROM debian:bullseye-slim

RUN apt-get update && apt-get install -y --no-install-recommends \
    git \
    wget \
    unzip \
    nodejs \
    npm \
    libgtk-3-0 \
    libx11-6 \
    libx11-xcb1 \
    libxt6 \
    libglib2.0-0 \
    libdbus-glib-1-2 \
    ca-certificates \
    && rm -rf /var/lib/apt/lists/* \
    && groupadd -g 1000 crawl \
    && useradd -d /home/crawl -s /bin/sh -m  -u 1000 -g 1000 crawl

USER crawl
WORKDIR /home/crawl

# Clone and patch web-ext
COPY --chown=crawl:crawl web-ext_support_multiple_source_dirs.patch /home/crawl/
RUN git clone --depth 1 --branch 5.5.0 https://github.com/mozilla/web-ext.git \
    && (cd web-ext \
        && git apply ../web-ext_support_multiple_source_dirs.patch \
        && npm install \
        && npm run build)

# Untar the crawl script and the crawl extension and install NPM modules
ARG crawltar
# Make the build variable mandatory
RUN test -n "${crawltar}"
COPY --chown=crawl:crawl "${crawltar}" /home/crawl/
RUN tar -xpf "${crawltar}" \
    && npm install

CMD ./run_crawl.sh crawl_urls.txt
