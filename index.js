/*
 * Copyright (c) 2022 Inria
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/*
 * WEB_EXT_SOURCE_DIRS=./how-broken node index.js crawl.txt
 */

import path from "path";
import { fileURLToPath } from "url";
import fs from "fs";
import puppeteer from "puppeteer-core";
import webExt from "web-ext";

const PAGE_DOMCONTENTLOADED_TIMEOUT_S = 20;
const PAGE_LOAD_TIMEOUT_S = 30;
const PAGE_WAIT_IDLE_S = 3;
const EXTENSION_ANALYSIS_TIMEOUT_S = 15;
const EXTENSION_HEADER_LOAD_TIMEOUT_S = 5;
const DATA_DIR = "data";
const REPORTS_DIR = "reports";

const HEADLESS = true;
const VIEWPORT = {
  width: 1280,
  height: 800,
};

const SECOND_TO_MS = 1000;

const FIREFOX_VERSION = "94.0a1";

const AVERAGE_MB_SIZE_PER_URL = 2;

const URL_DONE_INDEX_NAME = "URL_DONE_INDEX";

const ANALYSIS_REPORT_SELECTOR = "#js-ui-framework-detection";
const HEADER_LOG_REPORT_SELECTOR = "#http-headers-log";

const __dirname = path.dirname(fileURLToPath(import.meta.url));

const args = process.argv.slice(2);
const urlListFilepath = args[0];
if (!urlListFilepath) {
  console.error("Could not read the URL list filepath.");
  process.exit(1);
}

let outputDir = args[1];
if (!outputDir) {
  outputDir = __dirname;
}

console.log(`Will use ${outputDir} as output directory.`);

const urlList = fs.readFileSync(urlListFilepath)
  .toString()
  .split("\n")
  .filter((url) => Boolean(url));

console.log("Please make sure there is at least "
  + (AVERAGE_MB_SIZE_PER_URL * urlList.length / 1000).toLocaleString(
    "en-US",
    { maximumSignificantDigits: 3 },
  )
  + ` GB available on the filesystem where ${outputDir} is stored.`);

const crawlStartTime = Date.now();

const urlListName = path.parse(urlListFilepath).name;
const crawlOutputDir = path.resolve(
  outputDir,
  `crawl_${urlListName}_${crawlStartTime}`,
);
fs.mkdirSync(crawlOutputDir);

const logger = fs.createWriteStream(
  `${crawlOutputDir}/crawl_${urlListName}_${crawlStartTime}.log`,
  { flags: "a" },
);

const errorLogger = fs.createWriteStream(
  `${crawlOutputDir}/crawl_${urlListName}_${crawlStartTime}.error`,
  { flags: "a" },
);

const crawlLog = (message) => {
  const time = new Date().toISOString();
  const msg = `${time}: ${message}`;
  logger.write(msg + "\n");
  console.log(msg);
};

const crawlLogError = (message) => {
  const time = new Date().toISOString();
  const msg = `${time}: ERROR: ${message}`;
  errorLogger.write(msg + "\n");
  console.log(msg);
};

const START_INDEX_ENV_VAR = "START_INDEX";
const startIndexEnvVar = Number.parseInt(process.env[START_INDEX_ENV_VAR]);
if (process.env[START_INDEX_ENV_VAR] && Number.isNaN(startIndexEnvVar)) {
  console.error(`Could not parse the ${START_INDEX_ENV_VAR} environement \
variable.`);
  process.exit(1);
}
const startIndex = startIndexEnvVar ? startIndexEnvVar : 0;
crawlLog(`Will start at index ${startIndex} of the URL list.`);

const substituteURL = (url) => {
  return url.replace(/\//g, "_");
};

process.on("uncaughtException", (err) => {
  crawlLogError(`uncaught Exception at: ${err}`, "");
});

process.on("unhandledRejection", (reason, promise) => {
  crawlLogError(`unhandled Rejection at: ${promise} reason: ${reason}`, "");
});

const hrtimeToMs = (hrtime) => {
  const NS_PER_SEC = 1e9;
  const NS_TO_MS = 1e6;

  return Math.ceil((hrtime[0] * NS_PER_SEC + hrtime[1]) / NS_TO_MS);
};

const evaluateWithTimeout = (page, fn, otherArgs, timeout) => {
  const errMsg
    = `timeout of ${timeout} ms in evaluateWithTimeout`;

  return Promise.race([
    () => page.evaluate(fn, ...otherArgs),
    () => new Promise((resolve, reject) => {
      setTimeout(
        () => reject(new Error(errMsg)),
        timeout,
      );
    }),
  ]);
};

const saveHTML = async (page, filenameBase) => {
  // Write the page HTML to a file
  const htmlFilename = `${filenameBase}.html`;
  const htmlPath = `${crawlOutputDir}/${DATA_DIR}/${htmlFilename}`;
  fs.promises.writeFile(htmlPath, await page.content());
  crawlLog(`HTML page written for ${page.url()}.`);
  return htmlPath;
};

const saveJSONReport = async (page, filenameBase) => {
  const dataJSON = await page.$eval(
    ANALYSIS_REPORT_SELECTOR,
    (jsonScript) => jsonScript.textContent,
  );
  const jsonDataFilename = `data_${filenameBase}.json`;
  const jsonDataPath = `${crawlOutputDir}/${DATA_DIR}/${jsonDataFilename}`;
  fs.promises.writeFile(jsonDataPath, dataJSON);
  crawlLog("Data JSON written.");
  return jsonDataPath;
};

const saveJSONHeaderLog = async (page, filenameBase) => {
  const headerLogJSON = await page.$eval(
    HEADER_LOG_REPORT_SELECTOR,
    (jsonScript) => jsonScript.textContent,
  );
  const jsonHeaderLogFilename = `headers_${filenameBase}.json`;
  const jsonHeaderLogPath
    = `${crawlOutputDir}/${DATA_DIR}/${jsonHeaderLogFilename}`;
  fs.promises.writeFile(jsonHeaderLogPath, headerLogJSON);
  crawlLog("Data JSON written.");
  return jsonHeaderLogPath;
};

const analyseUrl = async (page, url, isLandingPage) => {
  page.setDefaultNavigationTimeout(PAGE_LOAD_TIMEOUT_S * SECOND_TO_MS);

  crawlLog(`Starting ${url}`);

  const startTime = process.hrtime();

  // Use a small timeout for the DOMContentLoaded event, to fail quickly when
  // possible
  await page.goto(url, {
    waitUntil: "domcontentloaded",
    timeout: PAGE_DOMCONTENTLOADED_TIMEOUT_S * SECOND_TO_MS,
  });
  const domLoadingTime = hrtimeToMs(process.hrtime(startTime));
  const realPageURL = page.url();
  crawlLog(`Loaded DOM ${realPageURL} for ${url} in ${domLoadingTime} ms.`);

  await page.waitForNavigation({
    waitUntil: "load",
    timeout: PAGE_LOAD_TIMEOUT_S * SECOND_TO_MS,
  });
  const pageLoadingTime = hrtimeToMs(process.hrtime(startTime));
  crawlLog(`Loaded page ${realPageURL} for ${url} in ${pageLoadingTime} ms.`);

  // Wait some time to make sure lazy-loaded resources are loaded
  await page.waitForTimeout(PAGE_WAIT_IDLE_S * SECOND_TO_MS);
  crawlLog(`Waited for ${PAGE_WAIT_IDLE_S} s.`);

  const inspectionStartTime = process.hrtime();

  await page.waitForSelector(ANALYSIS_REPORT_SELECTOR, {
    timeout: EXTENSION_ANALYSIS_TIMEOUT_S * SECOND_TO_MS,
  });
  await page.waitForSelector(HEADER_LOG_REPORT_SELECTOR, {
    timeout: EXTENSION_HEADER_LOAD_TIMEOUT_S * SECOND_TO_MS,
  });
  const extensionsInspectionTime
    = hrtimeToMs(process.hrtime(inspectionStartTime));
  crawlLog(
    `Found analysis done selector after ${extensionsInspectionTime} ms.`,
  );

  const filenameBase = `${substituteURL(page.url())}_${Date.now()}`;

  const jsonDataPath = await saveJSONReport(page, filenameBase);
  const jsonHeaderLogPath = await saveJSONHeaderLog(page, filenameBase);

  const htmlPath = await saveHTML(page, filenameBase);

  const crawlURLResultsJSON = JSON.stringify({
    crawlUrl: url,
    realPageURL,
    isLandingPage: Boolean(isLandingPage),
    jsonDataPath,
    jsonHeaderLogPath,
    htmlPath,
  });

  // Write the analysis report in a JSON file
  await fs.promises.writeFile(
    `${crawlOutputDir}/${REPORTS_DIR}/${substituteURL(url)}.json`,
    crawlURLResultsJSON,
  ).catch((err) => {
    crawlLogError(`${err} on ${url}`, "");
  });
};

const inspectWebsite = async (browser, url) => {
  const page = await browser.newPage();
  // Analyze the landing page
  await analyseUrl(page, url, true);

  const sampleLinks = await page.evaluate(() => {
    const currentUrl = new URL(document.location.href);

    // Durstenfeld shuffle
    const shuffleArray = (array) => {
      for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
      }
    };

    const links = Array.from(document.querySelectorAll("a"))
      .filter(a => a.href.length > 0)
      .filter(a => {
        const url = new URL(a.href);
        return url.origin === currentUrl.origin
          && url.pathname !== currentUrl.pathname;
    });

    shuffleArray(links);

    // Random sample of 3 links from the same origin pointing to a different
    // page
    return Promise.resolve(links.slice(0, 3).map((a) => a.href));
  });
  crawlLog(
    `Random sample of 3 links: ${sampleLinks}`,
  );

  for (const internalUrl of sampleLinks) {
    await analyseUrl(page, internalUrl);
  }

  await page.close();
};

const launchFirefox = async (
  revisionInfo,
  platform,
  cdpPort,
  pref,
  sourceDirs,
  headless,
) => {
  const cliArgs = ["--remote-debugging-port", cdpPort];
  if (headless) {
    cliArgs.unshift("--headless");
  }

  // For now, the upstream web-ext tool does not support multiple sourceDir
  // paths, but it is easy to patch it with the following:
  // diff --git a/src/cmd/run.js b/src/cmd/run.js
  // index 5334554..93eac0b 100644
  // --- a/src/cmd/run.js
  // +++ b/src/cmd/run.js
  // @@ -130 +130,5 @@ export default async function run(
  // -  const manifestData = await getValidatedManifest(sourceDir);
  // +  const extensions = await Promise.all(sourceDir.split(',')
  // +    .map(async (sourceDir) => {
  // +      const manifestData = await getValidatedManifest(sourceDir);
  // +      return { sourceDir, manifestData };
  // +    }));
  // @@ -154 +158 @@ export default async function run(
  // -    extensions: [{sourceDir, manifestData}],
  // +    extensions,
  const sourceDir = process.env.WEB_EXT_SOURCE_DIRS.split(',')
    .concat(sourceDirs)
    .flat()
    .map((sourceDirPath) => path.resolve(__dirname, sourceDirPath))
    .join(",");
  crawlLog(`sourceDir: ${sourceDir}`, "");

  await webExt.cmd.run({
    firefox: revisionInfo.executablePath,
    sourceDir,
    noReload: true, // Disable file watching
    pref,
    args: cliArgs,
  }, {
    shouldExitProgram: false,
  });

  const browserVersion = `${revisionInfo.product} ${revisionInfo.revision}`;
  crawlLog(`Launched ${browserVersion} on ${platform}.`);
};

const connectToFirefox = (cdpPort) => {
  return puppeteer.connect({
    browserURL: `http://localhost:${cdpPort}`,
    product: "firefox",
    defaultViewport: {
      width: VIEWPORT.width,
      height: VIEWPORT.height,
    },
  });
};

const getLastPage = async (browser) => {
  const pages = await browser.pages();
  return pages[pages.length - 1];
};

(async () => {
  const cdpPort = 12345;

  const browserFetcher = puppeteer.createBrowserFetcher({ product: "firefox" });
  crawlLog(`Downloading Firefox ${FIREFOX_VERSION}`, "");
  const revisionInfo = await browserFetcher.download(FIREFOX_VERSION);
  const platform = browserFetcher.platform();
  crawlLog("Firefox downloaded", "");

  await launchFirefox(
    revisionInfo,
    platform,
    cdpPort,
    {},
    [],
    HEADLESS,
  );

  await new Promise((resolve) => setTimeout(resolve, 2000));
  const browser = await connectToFirefox(cdpPort);

  fs.mkdirSync(`${crawlOutputDir}/${DATA_DIR}`);
  fs.mkdirSync(`${crawlOutputDir}/${REPORTS_DIR}`);

  crawlLog(`Data will be stored to ${crawlOutputDir}`, "all");

  /* eslint-disable no-await-in-loop */
  for (const [urlIndex, url] of urlList.slice(startIndex).entries()) {
     await Promise.all([
      inspectWebsite(browser, url)
        .catch(async (err) => {
          const page = await getLastPage(browser);
          crawlLogError(`${err} on ${page.url()} for ${url}`);
          page.close();
        }),
    ]);

    // Write the index of the last completed URL to a file, to be able to resume
    // at this index the crawl if needed
    await fs.promises.writeFile(
      `${crawlOutputDir}/${URL_DONE_INDEX_NAME}`,
      startIndex + urlIndex,
    ).catch((err) => {
      crawlLogError(`${err} on ${url}`, "");
    });

    crawlLog(`${startIndex + urlIndex + 1}/${urlList.length} done.`, "all");
  }
  /* eslint-enable no-await-in-loop */

  crawlLog("Done.", "");
})();
